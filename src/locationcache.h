/**
 * Copyright 2019 Nicolas Fella <nicolas.fella@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <QFile>
#include <QJsonArray>
#include <QObject>

#include <KPublicTransport/Location>

class LocationCache : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList cachedLocations READ cachedLocations WRITE setCachedLocations NOTIFY cachedLocationsChanged)

public:
    explicit LocationCache(QObject *parent = nullptr);

    QVariantList cachedLocations() const;
    void setCachedLocations(const QVariantList &locations);

    Q_INVOKABLE void addCachedLocation(const KPublicTransport::Location location);

Q_SIGNALS:
    void cachedLocationsChanged();

private:
    void loadLocationsFromCache();

    QVariantList m_cachedLocations;
    QFile m_locationCacheFile;
    QJsonArray m_cachedLocationsJson;
};
